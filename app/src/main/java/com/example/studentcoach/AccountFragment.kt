package com.example.studentcoach

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast

class AccountFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_account, container, false)

        //Initialise all widgets
        val btnImportData = view.findViewById<Button>(R.id.import_data_button)
        val btnDaily = view.findViewById<Button>(R.id.daily_button)
        val btnConnect = view.findViewById<Button>(R.id.connect_button)

        //Go to import data page if button clicked
        btnImportData.setOnClickListener {
            activity?.finish()
            startActivity(Intent(activity, SelectData :: class.java))
        }

        //Go to Daily Stats page if button clicked
        btnDaily.setOnClickListener {
            activity?.finish()
            startActivity(Intent(activity, ShowYesterdaysStats :: class.java))
        }

        //If 'Connect Wearable' button is clicked, it will display a message
        btnConnect.setOnClickListener {
            Toast.makeText(activity, "This function would be implemented in the future to track live data", Toast.LENGTH_LONG).show()
        }

        return view
    }

}
