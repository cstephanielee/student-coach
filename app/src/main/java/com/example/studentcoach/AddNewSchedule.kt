package com.example.studentcoach

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import io.realm.Realm
import io.realm.kotlin.createObject
import java.time.LocalDate
import java.time.ZoneId
import java.util.*

class AddNewSchedule : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_schedule)

        //Initialise all widgets
        val btnAddActivity = findViewById<Button>(R.id.add_activity_button)
        val btnBack = findViewById<Button>(R.id.go_back_button)
        val activityText = findViewById<EditText>(R.id.activity_editText)
        var date: LocalDate
        var dateReturn: Date

        val datePicker = findViewById<DatePicker>(R.id.datePicker)
        val cal = Calendar.getInstance() //Today's date

        datePicker.minDate = System.currentTimeMillis() - 1000 //Makes it so that users are unable to select a date in the past

        date = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH))
        dateReturn = java.sql.Date.valueOf(date.toString()) //Convert from LocalDate to Date


        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) //When date is changed on the datePicker
        ) { _, year, monthOfYear, dayOfMonth ->
            val month = monthOfYear + 1
            date = LocalDate.of(year, month, dayOfMonth)
            dateReturn = java.sql.Date.valueOf(date.toString()) //convert from LocalDate to Date
        }

        //Add activity to database if button is clicked
        btnAddActivity.setOnClickListener {
            if (activityText.text.toString().trim().isEmpty()) { //Not allowed to submit empty activity
                Toast.makeText(this@AddNewSchedule, "Please input an activity!", Toast.LENGTH_SHORT).show()
            }
            else {
                val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
                realm.use { realm ->
                    realm.beginTransaction()
                    val schedule = realm.createObject<Schedule>()
                    schedule.date = dateReturn
                    schedule.activity = activityText.text.toString()
                    realm.commitTransaction() //Adds activity to database

                    //Return back to Calendar page after activity is added
                    finish()
                    val intent = Intent(this, MainActivity :: class.java)
                    intent.putExtra("Fragment", "Calendar")
                    intent.putExtra("notification", "no")
                    startActivity(intent)
                }
            }
        }

        //Same as above but occurs if Enter key is pressed to add the activity
        activityText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyevent: KeyEvent): Boolean {
                return if (keyevent.getAction() === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {

                    if (activityText.text.toString().trim().isEmpty()) { //Activity box not allowed to be empty
                        Toast.makeText(this@AddNewSchedule, "Please input an activity!", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
                        realm.use { realm ->
                            realm.beginTransaction()
                            val schedule = realm.createObject<Schedule>()
                            schedule.date = dateReturn
                            schedule.activity = activityText.text.toString()
                            realm.commitTransaction() //Saves activity to database

                            finish()
                            val intent = Intent(this@AddNewSchedule, MainActivity :: class.java)
                            intent.putExtra("Fragment", "Calendar")
                            intent.putExtra("notification", "no")
                            startActivity(intent)
                        }
                    }
                    true
                } else false
            }
        })

        //Return to Calendar page if button is clicked
        btnBack.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Calendar")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

    }
}
