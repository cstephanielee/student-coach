package com.example.studentcoach

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import org.w3c.dom.Text
import java.io.BufferedReader
import java.io.InputStreamReader

class BadgeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_badge)

        //Initalise all widgets
        val badgeLevel = findViewById<TextView>(R.id.badge_level_text)
        val progressBar = findViewById<ProgressBar>(R.id.progress_bar)
        val xpLevel = findViewById<TextView>(R.id.xp_text)
        val btnBack = findViewById<Button>(R.id.go_back_button)

        //Get data passed from other fragments/activities
        val friendName = intent.getStringExtra("name")
        val returnWhere = intent.getStringExtra("Return")

        val dataBadgeData = readFile(friendName) //Read data of particular person

        //Set their stats on the widgets
        badgeLevel.text = dataBadgeData[0]
        progressBar.progress = dataBadgeData[1].toInt()
        xpLevel.text = "${dataBadgeData[1]}/100 XP"

        //Returns back to the relevant page depending on where it was clicked from
        btnBack.setOnClickListener {
            finish()
            if (returnWhere == "Social") {
                val intent = Intent(this, MainActivity :: class.java)
                intent.putExtra("Fragment", "Social")
                intent.putExtra("notification", "no")
                startActivity(intent)
            }
            else {
                val intent = Intent(this, ShowFriendsStatsActivity :: class.java)
                intent.putExtra("name", friendName)
                startActivity(intent)
            }
        }

    }

    //Read friends data
    private fun readFile (person: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_NAME = 0
        val DATA_BADGE = 1
        val DATA_STEPS_SO_FAR = 2
        val DATA_WATER = 3
        val DATA_XP_LEVEL = 4

        try {
            val friendData = ArrayList<FriendData>()
            var lineOfData: String?
            fileReader = BufferedReader(InputStreamReader(this!!.assets.open("friends_data.csv")))
            fileReader.readLine() //Get rid of header
            lineOfData = fileReader.readLine()
            while (lineOfData != null) {
                val splitData = lineOfData.split(",")
                if (splitData.isNotEmpty()) {
                    val friendDataObject = FriendData(
                        splitData[DATA_NAME],
                        Integer.parseInt(splitData[DATA_BADGE]),
                        Integer.parseInt(splitData[DATA_STEPS_SO_FAR]),
                        Integer.parseInt(splitData[DATA_WATER]),
                        Integer.parseInt(splitData[DATA_XP_LEVEL]))
                    friendData.add(friendDataObject)
                    if (person == friendDataObject.name) {
                        arrayReturn.add(splitData[DATA_BADGE])
                        arrayReturn.add(splitData[DATA_XP_LEVEL])
                        return arrayReturn
                    }
                }
                lineOfData = fileReader.readLine()
            }

        } catch (e: Exception) {
            println(e.printStackTrace())
        }
        return arrayReturn
    }
}
