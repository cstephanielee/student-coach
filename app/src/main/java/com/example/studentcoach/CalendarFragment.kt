package com.example.studentcoach

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import io.realm.kotlin.where
import java.time.LocalDate
import java.time.ZoneId
import java.util.*


class CalendarFragment : Fragment() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_calendar, container, false)
        //Initialise widgets
        var newActivityBtn = view.findViewById<Button>(R.id.add_new_activity_btn)
        val linearLayout = view.findViewById<LinearLayout>(R.id.Schedule_layout)

        //Get an instance of database to use
        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())

        //Sort the activities from the database in ascending order by date
        val sortedList: RealmResults<Schedule> = realm.where(Schedule::class.java)
            .sort("date", Sort.ASCENDING)
            .findAll()

        var date:LocalDate?
        val cal = Calendar.getInstance()
        val today = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH))

        //Goes through all the schedules in the database
        for (schedule in sortedList) {
            date = schedule.date?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDate()
            if (date != null) {
                if (date < today) { //Checks if date of activity is in the past and deletes it if so
                    realm.beginTransaction()
                    var pastDates = realm.where<Schedule>().equalTo("date", schedule.date).findAll()
                    pastDates.deleteAllFromRealm()
                    realm.commitTransaction()
                }
                else { //creates a button with the activity on it
                    var btnNew: Button = Button(activity)
                    val dateFormattedWithActivity = date.dayOfMonth.toString() + "/" + date.monthValue + "/" + date.year + " - " + schedule.activity
                    btnNew.text = dateFormattedWithActivity
                    linearLayout.addView(btnNew)

                    //New schedule button will go to the modify or delete page when clicked
                    btnNew.setOnClickListener {
                        val intent = Intent(activity, ModifyOrDeleteSchedule :: class.java)
                        intent.putExtra("date", schedule.date?.toInstant()?.atZone(ZoneId.systemDefault())?.toLocalDate().toString())
                        intent.putExtra("activity", schedule.activity)
                        startActivity(intent)
                    }
                }
            }
        }

        //Goes to Add new schedule page if clicked
        newActivityBtn.setOnClickListener {
            startActivity(Intent(activity, AddNewSchedule ::class.java))
        }

        return view
    }
}

