package com.example.studentcoach

import io.realm.RealmObject

open class DatePicked : RealmObject() {
    var dateChosen: String? = null
}