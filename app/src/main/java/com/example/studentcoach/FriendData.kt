package com.example.studentcoach

class FriendData {

    var name: String? = null
    private var badgeLevel: Int = 0
    private var stepsSoFar: Int = 0
    private var water: Int = 0
    private var xpLevel: Int = 0

    constructor() {}
    constructor(name: String, badgeLevel: Int, stepsSoFar: Int, water: Int, xpLevel: Int) {
        this.name = name
        this.badgeLevel = badgeLevel
        this.stepsSoFar = stepsSoFar
        this. water = water
        this.xpLevel = xpLevel
    }
}