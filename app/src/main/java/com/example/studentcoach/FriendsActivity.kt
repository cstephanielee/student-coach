package com.example.studentcoach

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class FriendsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_friends)

        //Initialise widgets
        val btnFirstPlace = findViewById<Button>(R.id.first_place_button)
        val btnSecondPlace = findViewById<Button>(R.id.second_place_button)
        val btnThirdPlace = findViewById<Button>(R.id.third_place_button)
        val btnBack = findViewById<Button>(R.id.go_back_button)

        //Go to show friends stats if clicked
        btnFirstPlace.setOnClickListener {
            finish()
            val intent = Intent(this, ShowFriendsStatsActivity :: class.java)
            intent.putExtra("name", "Alan Mello")
            startActivity(intent)
        }

        //Go to show friends stats page if button clicked
        btnSecondPlace.setOnClickListener {
            finish()
            val intent = Intent(this, ShowFriendsStatsActivity :: class.java)
            intent.putExtra("name", "Kim Tae-hyung")
            startActivity(intent)

        }

        //Go back to social page if button clicked
        btnThirdPlace.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Social")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

        //Go back to social page if button clicked
        btnBack.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Social")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

    }
}
