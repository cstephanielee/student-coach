package com.example.studentcoach

import java.sql.Time
import java.time.DayOfWeek
import java.time.Month
import java.time.Year

class HealthData {

    var day: String? = null
    private var month: String? = null
    private var year: String? = null
    private var stepCount: Int = 0
    private var countSoFar: Int = 0
    private var sleepTime: String? = null
    private var wakeTime: String? = null
    private var sleepDuration: String? = null
    private var heartRate:String? = null
    private var water: String? = null

    constructor() {}
    constructor(day: String, month: String, year: String, stepCount: Int, countSoFar: Int, sleepTime: String, wakeTime: String, sleepDuration: String, heartRate: String, water: String) {
        this.day = day
        this.month = month
        this.year = year
        this.stepCount = stepCount
        this.countSoFar = countSoFar
        this.sleepTime = sleepTime
        this.wakeTime = wakeTime
        this.sleepDuration = sleepDuration
        this.heartRate = heartRate
        this.water = water
    }

    override fun toString(): String {
        return "Data [day=$day, month=$month, year=$year, stepCount=$stepCount, countSoFar=$countSoFar, sleepTime=$sleepTime, wakeTime=$wakeTime, sleepDuration=$sleepDuration, heartRate=$heartRate, water=$water]"
    }
}
