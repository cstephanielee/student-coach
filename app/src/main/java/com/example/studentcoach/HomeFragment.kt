package com.example.studentcoach

import android.app.ActionBar
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import io.realm.Realm
import io.realm.kotlin.where
import java.io.BufferedReader
import java.io.InputStreamReader


class HomeFragment : Fragment() {

    private var dateSelected: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        //Get instance of database to use
        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
        //Get current date of data being viewed
        for (date in realm.where<DatePicked>().findAll()) {
            dateSelected = date?.dateChosen.toString()
        }

        //Initialise waterCount widget
        val waterCount = view.findViewById<TextView>(R.id.water_count_text)

        if (dateSelected != "") {
            //Initialise other widgets
            val dateText = view.findViewById<TextView>(R.id.date_text)
            val stepsText = view.findViewById<TextView>(R.id.steps_text)
            val sleepText = view.findViewById<TextView>(R.id.sleep_duration_text)
            val heartText = view.findViewById<TextView>(R.id.heart_rate_text)

            var getData = readFile(dateSelected) //Get data for current date
            var hours: String
            var mins: String

            //split the duration into hours and minutes
            var splitString: List<String> = getData[1].split(":")
            if (splitString[0][0] == '0') {
                hours = if (splitString[0][1] == '0') {
                    '0'.toString()
                } else {
                    splitString[0][1].toString()

                }
            }
            else {
                hours = splitString[0]
            }
            if (splitString[1][0] == '0') {
                mins = if (splitString[1][1] == '0') {
                    '0'.toString()
                } else {
                    splitString[1][1].toString()
                }
            }
            else {
                mins = splitString[1]
            }
            //Set data to widgets
            stepsText.text = getData[0]
            sleepText.text = hours + "hrs " + mins + "mins"
            heartText.text = getData[2] + " bpm"
            waterCount.text = getData[3]
            dateText.text = "Data from: $dateSelected"
        }

        //initialise water button widgets
        var waterPlusBtn = view.findViewById<ImageButton>(R.id.plus_water_button)
        var waterMinusBtn = view.findViewById<ImageButton>(R.id.minus_water_button)

        //Increases water count if clicked
        waterPlusBtn.setOnClickListener {
            var intCount:Int = waterCount.text.toString().toInt()
            intCount += 1
            waterCount.text = intCount.toString()
        }
        //Decreases water count if clicked
        waterMinusBtn.setOnClickListener {
            var intCount:Int = waterCount.text.toString().toInt()
            if (intCount == 0) {
                Toast.makeText(activity, "You can't drink a negative amount of water!", Toast.LENGTH_LONG).show()
            }
            else{
                intCount -= 1
                waterCount.text = intCount.toString()
            }
        }

        return view
    }

    //Get data for current date
    private fun readFile (dateSelected: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_DAY = 0
        val DATA_MONTH = 1
        val DATA_YEAR = 2
        val DATA_STEP_COUNT = 3
        val DATA_COUNT_SO_FAR = 4
        val DATA_SLEEP_TIME = 5
        val DATA_WAKE_TIME = 6
        val DATA_SLEEP_DURATION = 7
        val DATA_HEART_RATE = 8
        val DATA_WATER = 9

        try {
            val healthData = ArrayList<HealthData>()
            var dayLine: String?
            var dateDay:String = ""
            fileReader = BufferedReader(InputStreamReader(context!!.assets.open("health_data.csv")))
            fileReader.readLine() //Get rid of header
            dayLine = fileReader.readLine()
            while (dayLine != null) {
                val splitData = dayLine.split(",")
                if (splitData.isNotEmpty()) {
                    val dayData = HealthData(
                        splitData[DATA_DAY],
                        splitData[DATA_MONTH],
                        splitData[DATA_YEAR],
                        Integer.parseInt(splitData[DATA_STEP_COUNT]),
                        Integer.parseInt(splitData[DATA_COUNT_SO_FAR]),
                        splitData[DATA_SLEEP_TIME],
                        splitData[DATA_WAKE_TIME],
                        splitData[DATA_SLEEP_DURATION],
                        splitData[DATA_HEART_RATE],
                        splitData[DATA_WATER])
                    healthData.add(dayData)
                    if (dateSelected[0] == '0') {
                        dateDay = dateSelected[1].toString()
                    }
                    else {
                        dateDay = dateSelected[0].toString() + dateSelected[1].toString()
                    }
                    if (dayData.day == dateDay) {
                        arrayReturn.add(splitData[DATA_STEP_COUNT])
                        arrayReturn.add(splitData[DATA_SLEEP_DURATION])
                        arrayReturn.add(splitData[DATA_HEART_RATE])
                        arrayReturn.add(splitData[DATA_WATER])
                        return arrayReturn
                    }
                }
                dayLine = fileReader.readLine()
            }
        } catch (e: Exception) {
            println(e.printStackTrace())
        }
        return arrayReturn
    }

}
