package com.example.studentcoach

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.OkHttpClient
import org.json.JSONArray
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class MainActivity : AppCompatActivity() {

    //private val client = OkHttpClient() //Unused code to get api to work

    private var quote:String = "Stop creating a life that you need a vacation from. Instead move to where you want to live, do what you want to do, start what you want to start and create the life you want today. This isn't rehearsal people. This is YOUR life. - Dale Patridge"

    //Dictates where you go when you click on the bottom navigation bar
    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener {item ->
        when(item.itemId){
            R.id.navigation_home -> {
                println("home pressed")
                replaceFragment(HomeFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_social -> {
                println("social pressed")
                replaceFragment(SocialFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_calendar -> {
                println("calendar pressed")
                replaceFragment(CalendarFragment())
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_account -> {
                println("account pressed")
                replaceFragment(AccountFragment())
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Initialise the database
        Realm.init(this)
        val config = RealmConfiguration.Builder().name("StudentCoachDatabase").build()
        Realm.setDefaultConfiguration(config)

        //RetrieveFeedTask().execute() //Unused code to get quote from api

        //Get data passed from other pages to decide whether to produce notification
        val notificationShow = intent.getStringExtra("notification")

        if (notificationShow == null) {
            createNotificationChannel()
            //Create first notification - inspirational quote and reminder to drink water
            createNotification1()
            //Create notification 2 - data analysis stats
            createNotification2()
        }

        //Get data passed from other pages to determine which page to go to next
        val goToFragment: String? = intent.getStringExtra("Fragment")

        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if (goToFragment == "Calendar") {
            replaceFragment(CalendarFragment())
        }
        else if (goToFragment == "Account") {
            replaceFragment(AccountFragment())
        }
        else if (goToFragment == "Social") {
            replaceFragment(SocialFragment())
        }
        else {
            replaceFragment(HomeFragment())
        }
    }

    //Changes pages
    private fun replaceFragment(fragment: Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.fragmentContainerAll, fragment)
        fragmentTransaction.commit()
    }

    //Creates the channel for notifications
    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Student Coach"
            val descriptionText = "This is a notification"
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel("1234", name, importance).apply {
                description = descriptionText
            }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    //Creates the first notification
    private fun createNotification1() {
        val intentNotification1 = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent1: PendingIntent = PendingIntent.getActivity(this, 0, intentNotification1, 0)

        var builder1 = NotificationCompat.Builder(this, "1234")
            .setSmallIcon(R.drawable.logo_clear_bg)
            .setContentTitle("Good Morning!")
            .setContentText("$quote\nRemember to drink water!")
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText("$quote\nRemember to drink water!"))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent1)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, builder1.build())
        }
    }

    //Creates the second notification
    private fun createNotification2() {
        val stats = produceStats()

        val intentNotification2 = Intent(this, ShowYesterdaysStats::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intentNotification2, 0)

        var builder2 = NotificationCompat.Builder(this, "1234")
            .setSmallIcon(R.drawable.logo_clear_bg)
            .setContentTitle("Daily Stats Report")
            .setContentText(stats)
            .setStyle(NotificationCompat.BigTextStyle()
                .bigText(stats))
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(2, builder2.build())
        }
    }

    //Get stats from previous day to be displayed on notification
    private fun produceStats(): String {
        var dateSelected:String = ""
        var yesterday: String = ""
        var statsSummary: String = ""
        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
        for (date in realm.where<DatePicked>().findAll()) {
            dateSelected = date?.dateChosen.toString()
        }

        var day:String = "${dateSelected[0]}${dateSelected[1]}"
        var dayInt = day.toInt()
        //Go back a day
        if (dayInt > 1) {
            dayInt -= 1
        }
        if (dayInt < 10) {
            yesterday = "0$dayInt/04/2018"
        }
        else {
            yesterday = "$dayInt/04/2018"
        }
        val getData = readFile(yesterday)
        val sleepDuration = processTime(getData[2])
        statsSummary =
            "Yesterday you:\n" +
                    "Achieved ${getData[0]} steps\n" +
                    "Slept at ${getData[1]}\n" +
                    "Slept for ${sleepDuration[0]}hrs ${sleepDuration[1]}mins\n" +
                    "Had an average heart rate of ${getData[3]}bpm\n" +
                    "Drank ${getData[4]} cups of water\n" +
                    "Click to view analysis"
        return statsSummary
    }

    //Get data for previous day
    private fun readFile (dateSelected: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_DAY = 0
        val DATA_MONTH = 1
        val DATA_YEAR = 2
        val DATA_STEP_COUNT = 3
        val DATA_COUNT_SO_FAR = 4
        val DATA_SLEEP_TIME = 5
        val DATA_WAKE_TIME = 6
        val DATA_SLEEP_DURATION = 7
        val DATA_HEART_RATE = 8
        val DATA_WATER = 9

        try {
            val healthData = ArrayList<HealthData>()
            var dayLine: String?
            var dateDay:String = ""
            fileReader = BufferedReader(InputStreamReader(this.assets.open("health_data.csv")))
            fileReader.readLine() //Get rid of header
            dayLine = fileReader.readLine()
            while (dayLine != null) {
                val splitData = dayLine.split(",")
                if (splitData.isNotEmpty()) {
                    val dayData = HealthData(
                        splitData[DATA_DAY],
                        splitData[DATA_MONTH],
                        splitData[DATA_YEAR],
                        Integer.parseInt(splitData[DATA_STEP_COUNT]),
                        Integer.parseInt(splitData[DATA_COUNT_SO_FAR]),
                        splitData[DATA_SLEEP_TIME],
                        splitData[DATA_WAKE_TIME],
                        splitData[DATA_SLEEP_DURATION],
                        splitData[DATA_HEART_RATE],
                        splitData[DATA_WATER])
                    healthData.add(dayData)
                    dateDay = if (dateSelected[0] == '0') {
                        dateSelected[1].toString()
                    } else {
                        dateSelected[0].toString() + dateSelected[1].toString()
                    }
                    if (dayData.day == dateDay) {
                        arrayReturn.add(splitData[DATA_STEP_COUNT])
                        arrayReturn.add(splitData[DATA_SLEEP_TIME])
                        arrayReturn.add(splitData[DATA_SLEEP_DURATION])
                        arrayReturn.add(splitData[DATA_HEART_RATE])
                        arrayReturn.add(splitData[DATA_WATER])
                        return arrayReturn
                    }
                }
                dayLine = fileReader.readLine()
            }
        } catch (e: Exception) {
            println(e.printStackTrace())
        }

        return arrayReturn
    }

    //Split hours and minutes in duration
    private fun processTime(time: String) : MutableList<String> {
        var hours: String
        var mins: String
        var arrayReturn = mutableListOf<String>()

        var splitString: List<String> = time.split(":")
        if (splitString[0][0] == '0') {
            hours = if (splitString[0][1] == '0') {
                '0'.toString()
            } else {
                splitString[0][1].toString()

            }
        }
        else {
            hours = splitString[0]
        }
        if (splitString[1][0] == '0') {
            mins = if (splitString[1][1] == '0') {
                '0'.toString()
            } else {
                splitString[1][1].toString()
            }
        }
        else {
            mins = splitString[1]
        }
        arrayReturn.add(hours)
        arrayReturn.add(mins)
        return arrayReturn
    }

    //Unused code for getting inspirational quote of the day from api
    /*private inner class RetrieveFeedTask : AsyncTask<Void, Void, Void>() {
        var string: String = ""
        override fun doInBackground(vararg params: Void?): Void? {
            val url = URL("https://quotes.rest/qod?category=funny")

            try {
                //make connection
                val urlc =
                    url.openConnection() as HttpURLConnection
                urlc.requestMethod = "GET"
                // set the content type
                urlc.setRequestProperty("Content-Type", "application/json")
                println("Connect to: $url")
                urlc.allowUserInteraction = false
                urlc.connect()

                //get result
                val br =
                    BufferedReader(InputStreamReader(urlc.inputStream))
                var line: String? = null
                while (br.readLine().also { line = it } != null) {
                    println(line)
                    string += line.toString()
                }
                br.close()
            } catch (e: java.lang.Exception) {
                println("Error occured")
                println(e.toString())
            }
            return null
        }
        override fun onPostExecute(aVoid: Void?) {
            resultString = string
            super.onPostExecute(aVoid)
        }
    }*/

}

//“Be the change that you wish to see in the world.”

