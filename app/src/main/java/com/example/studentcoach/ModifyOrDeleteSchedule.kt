package com.example.studentcoach

import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.time.LocalDate
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

class ModifyOrDeleteSchedule : AppCompatActivity() {

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_modify_or_delete_schedule)

        //Get data passed from other pages
        val activityPicked: String? = intent.getStringExtra("activity")
        val datePicked: String? = intent.getStringExtra("date") //In format of y-m-d

        //Format date variables
        val datePickedAsLocalDate:LocalDate = LocalDate.parse(datePicked, DateTimeFormatter.ISO_DATE)
        val datePickedAsDate = java.sql.Date.valueOf(datePickedAsLocalDate.toString())

        //Initialise widgets
        val btnUpdateActivity = findViewById<Button>(R.id.update_activity_button)
        val btnDeleteActivity = findViewById<Button>(R.id.delete_activity_button)
        val btnBack = findViewById<Button>(R.id.go_back_button)
        var title = findViewById<TextView>(R.id.activity_name_text)
        val activityText = findViewById<EditText>(R.id.activity_editText)
        var date: LocalDate
        var dateReturn: Date

        val datePicker = findViewById<DatePicker>(R.id.datePicker)
        val cal = Calendar.getInstance()

        title.text = "${datePickedAsLocalDate.dayOfMonth}/${datePickedAsLocalDate.monthValue}/${datePickedAsLocalDate.year} - $activityPicked"

        datePicker.minDate = System.currentTimeMillis() - 1000 //Makes it so that users are unable to select a date in the past

        date = LocalDate.of(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH)+1, cal.get(Calendar.DAY_OF_MONTH))
        dateReturn = java.sql.Date.valueOf(date.toString()) //Convert from LocalDate to Date


        datePicker.init(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH) //When date is changed on the datePicker
        ) { _, year, monthOfYear, dayOfMonth ->
            val month = monthOfYear + 1
            date = LocalDate.of(year, month, dayOfMonth)
            dateReturn = java.sql.Date.valueOf(date.toString()) //convert from LocalDate to Date
        }

        //Updates details of the activity in database if button clicked
        btnUpdateActivity.setOnClickListener { //Add activity
            if (activityText.text.toString().trim().isEmpty()) { //Not allowed to submit empty activity
                Toast.makeText(this@ModifyOrDeleteSchedule, "Please input an activity!", Toast.LENGTH_SHORT).show()
            }
            else {
                val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
                realm.use { realm ->
                    realm.executeTransaction {
                        val schedule = realm.where<Schedule>().equalTo("date", datePickedAsDate).and().equalTo("activity", activityPicked).findFirst()
                        schedule?.date = dateReturn
                        schedule?.activity = activityText.text.toString()
                    }

                    //Return back to calendar page
                    finish()
                    val intent = Intent(this, MainActivity :: class.java)
                    intent.putExtra("Fragment", "Calendar")
                    intent.putExtra("notification", "no")
                    startActivity(intent)
                }
            }
        }

        //Same as above but occurs when Enter key is pressed
        activityText.setOnKeyListener(object : View.OnKeyListener {
            override fun onKey(view: View?, keyCode: Int, keyevent: KeyEvent): Boolean {
                return if (keyevent.getAction() === KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {

                    if (activityText.text.toString().trim().isEmpty()) { //Not allowed to submit empty activity
                        Toast.makeText(this@ModifyOrDeleteSchedule, "Please input an activity!", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
                        realm.use { realm ->
                            realm.executeTransaction {
                                val schedule = realm.where<Schedule>().equalTo("date", datePickedAsDate).and().equalTo("activity", activityPicked).findFirst()
                                schedule?.date = dateReturn
                                schedule?.activity = activityText.text.toString()
                            }

                            //Return to calendar page
                            finish()
                            val intent = Intent(this@ModifyOrDeleteSchedule, MainActivity :: class.java)
                            intent.putExtra("Fragment", "Calendar")
                            intent.putExtra("notification", "no")
                            startActivity(intent)
                        }
                    }
                    true
                } else false
            }
        })

        //Delete activity from database if button clicked
        btnDeleteActivity.setOnClickListener {
            val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
            realm.beginTransaction()
            var deleteThisActivity = realm.where<Schedule>().equalTo("date", datePickedAsDate).and().equalTo("activity", activityPicked).findAll()
            deleteThisActivity.deleteAllFromRealm()
            realm.commitTransaction()

            //Return to calendar page
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Calendar")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

        //Go back to calendar page if clicked
        btnBack.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Calendar")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

    }
}
