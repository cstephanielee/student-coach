package com.example.studentcoach

import io.realm.RealmObject
import java.time.LocalDate
import java.util.*

open class Schedule : RealmObject() {
    var date: Date? = null
    var activity: String? = null
}