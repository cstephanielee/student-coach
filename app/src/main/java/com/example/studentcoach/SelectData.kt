package com.example.studentcoach

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where

class SelectData : AppCompatActivity() {

    //values for dropdown menu
    private val staticSet = setOf(
        "01/04/2018", "02/04/2018", "03/04/2018", "04/04/2018", "05/04/2018",
        "06/04/2018", "07/04/2018", "08/04/2018", "09/04/2018", "10/04/2018",
        "11/04/2018", "12/04/2018", "13/04/2018", "14/04/2018", "15/04/2018",
        "16/04/2018", "17/04/2018", "18/04/2018", "19/04/2018", "20/04/2018",
        "21/04/2018", "22/04/2018", "23/04/2018", "24/04/2018", "25/04/2018",
        "26/04/2018", "27/04/2018", "28/04/2018", "29/04/2018", "30/04/2018"
    )

    private lateinit var spinner: Spinner
    private var dateSelected:String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_data)

        //Initialise spinner
        spinner = findViewById((R.id.select_date_spinner))

        //Give values to the dropdown menu
        populateSpinner()

        //Initialise other widgets
        val btnPickDate = findViewById<Button>(R.id.pick_date_button)
        val btnGoBack = findViewById<Button>(R.id.go_back_button)

        //Selects the date chosen and sets it as the current date in database if clicked
        btnPickDate.setOnClickListener {
            dateSelected = spinner.selectedItem.toString()
            val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
            realm.use { realm ->
                realm.beginTransaction()
                for (date in realm.where<DatePicked>().findAll()) {
                    date.deleteFromRealm()
                }
                val date = realm.createObject<DatePicked>()
                date.dateChosen = dateSelected
                realm.commitTransaction()
            }

            //Returns to home page to view stats for new date chosen
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("notification", "no")
            startActivity(intent)

        }

        //Returns to account page if clicked
        btnGoBack.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("Fragment", "Account")
            intent.putExtra("notification", "no")
            startActivity(intent)
        }


    }

    //Gives the spinner values
    private fun populateSpinner() {
        ArrayAdapter( //create the adapter for dropdown menu
            this,
            android.R.layout.simple_spinner_item,
            staticSet.toTypedArray()
        ).also {
            adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter //set the adapter to the dropdown menu
        }
    }
}
