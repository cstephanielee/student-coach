package com.example.studentcoach

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import java.io.BufferedReader
import java.io.InputStreamReader

class ShowFriendsStatsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_friends_stats)

        //Initialise widgets
        val friendNameText = findViewById<TextView>(R.id.friends_name_text)
        val btnFriendBadge = findViewById<Button>(R.id.badge_button)
        val forestLayout = findViewById<LinearLayout>(R.id.Forest_Layout)
        val treeCountText = findViewById<TextView>(R.id.tree_count_text)
        val btnForestInfo = findViewById<Button>(R.id.forest_info_image)
        val forestText = findViewById<TextView>(R.id.forest_info_text)
        val waterImage = findViewById<ImageView>(R.id.water_image)
        val btnWaterInfo = findViewById<Button>(R.id.water_info_image)
        val waterText = findViewById<TextView>(R.id.water_info_text)
        val btnBack = findViewById<Button>(R.id.go_back_button)

        //Get the name of friend chosen from other page
        val friendName = intent.getStringExtra("name")
        //Get data of chosen friend
        val friendData = readFile(friendName)

        //Setting friend name
        friendNameText.text = friendName

        //Setting friend badge level
        btnFriendBadge.text = "BADGE LVL ${friendData[1]}"

        //Making Forest
        val countSoFar = friendData[2]
        val amountOfTrees = countSoFar.toInt()/10000

        for (x in 1 until amountOfTrees+1) {
            val treeImage = ImageView(this)
            treeImage.layoutParams = LinearLayout.LayoutParams(160,160)
            if (x%2 == 0) {
                treeImage.setImageResource(R.drawable.tree)
            }
            else {
                treeImage.setImageResource(R.drawable.tree2)
            }
            forestLayout.addView(treeImage)
        }
        treeCountText.text = "Total Trees: $amountOfTrees"

        //Filling up glass with water
        val waterCount = friendData[3].toInt()
        when (waterCount) {
            0 -> waterImage.setImageResource(R.drawable.glass0)
            1 -> waterImage.setImageResource(R.drawable.glass1)
            2 -> waterImage.setImageResource(R.drawable.glass2)
            3 -> waterImage.setImageResource(R.drawable.glass3)
            4 -> waterImage.setImageResource(R.drawable.glass4)
            5 -> waterImage.setImageResource(R.drawable.glass5)
            6 -> waterImage.setImageResource(R.drawable.glass6)
            7 -> waterImage.setImageResource(R.drawable.glass7)
            else -> {
                waterImage.setImageResource(R.drawable.glass8)
            }
        }

        //Go to page which shows friends badge activity if clicked
        btnFriendBadge.setOnClickListener {
            finish()
            val intent = Intent(this, BadgeActivity::class.java)
            intent.putExtra("name", friendName)
            startActivity(intent)
        }

        //Displays and hides information when clicked
        btnForestInfo.setOnClickListener {
            forestText.visibility =
                if (forestText.visibility == View.VISIBLE) {
                    View.INVISIBLE
                } else {
                    View.VISIBLE
                }
        }
        //Displays and hides information when clicked
        btnWaterInfo.setOnClickListener {
            waterText.visibility =
                if (waterText.visibility == View.VISIBLE) {
                    View.INVISIBLE
                } else {
                    View.VISIBLE
                }
        }

        //Go back to Leader Board page
        btnBack.setOnClickListener {
            finish()
            startActivity(Intent(this, FriendsActivity :: class.java))
        }

    }

    //Get friends data
    private fun readFile (person: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_NAME = 0
        val DATA_BADGE = 1
        val DATA_STEPS_SO_FAR = 2
        val DATA_WATER = 3
        val DATA_XP_LEVEL = 4

        try {
            val friendData = ArrayList<FriendData>()
            var lineOfData: String?
            fileReader = BufferedReader(InputStreamReader(this!!.assets.open("friends_data.csv")))
            fileReader.readLine() //Get rid of header
            lineOfData = fileReader.readLine()
            while (lineOfData != null) {
                val splitData = lineOfData.split(",")
                if (splitData.isNotEmpty()) {
                    val friendDataObject = FriendData(
                        splitData[DATA_NAME],
                        Integer.parseInt(splitData[DATA_BADGE]),
                        Integer.parseInt(splitData[DATA_STEPS_SO_FAR]),
                        Integer.parseInt(splitData[DATA_WATER]),
                        Integer.parseInt(splitData[DATA_XP_LEVEL]))
                    friendData.add(friendDataObject)
                    if (person == friendDataObject.name) {
                        arrayReturn.add(splitData[DATA_NAME])
                        arrayReturn.add(splitData[DATA_BADGE])
                        arrayReturn.add(splitData[DATA_STEPS_SO_FAR])
                        arrayReturn.add(splitData[DATA_WATER])
                        return arrayReturn
                    }
                }
                lineOfData = fileReader.readLine()
            }

        } catch (e: Exception) {
            println(e.printStackTrace())
        }
        return arrayReturn
    }
}
