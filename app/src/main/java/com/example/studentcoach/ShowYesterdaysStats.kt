package com.example.studentcoach

import android.content.Intent
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_show_yesterdays_stats.*
import java.io.BufferedReader
import java.io.InputStreamReader

class ShowYesterdaysStats : AppCompatActivity() {

    //Thresholds for data analysis
    private val STEPS_BAD = 5000
    private val STEPS_GOOD = 9000
    private val STEPS_EXCELLENT = 12500
    private val SLEEP_TIME_BAD_UNDER = 7
    private val SLEEP_TIME_BAD_OVER = 9
    private val HEART_RATE_BAD = 100
    private val HEART_AVG_BAD = 80
    private val HEART_AVG_GOOD = 60
    private val WATER = 8

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_yesterdays_stats)

        //Initialise widgets
        var layout = findViewById<LinearLayout>(R.id.analysis_scroll)
        var dateText = findViewById<TextView>(R.id.yesterday_text)
        val btnHome = findViewById<Button>(R.id.go_home_button)

        var dateSelected:String = ""
        var yesterday: String = ""

        //Get instance of database to use
        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
        for (date in realm.where<DatePicked>().findAll()) {
            dateSelected = date?.dateChosen.toString()
        }

        //Get previous days date
        var day:String = "${dateSelected[0]}${dateSelected[1]}"
        var dayInt = day.toInt()
        if (dayInt > 1) {
            dayInt -= 1
        }
        if (dayInt < 10) {
            yesterday = "0$dayInt/04/2018"
        }
        else {
            yesterday = "$dayInt/04/2018"
        }

        dateText.text = "($yesterday) Yesterday you:"

        //Get data of previous day
        val getData = readFile(yesterday)
        val sleepDuration = processTime(getData[2])
        val statsText = mutableListOf<String>()

        //Add stats of previous day to string
        statsText.add("*Achieved ${getData[0]} steps")
        statsText.add("*Slept at ${getData[1]}")
        statsText.add("*Slept for ${sleepDuration[0]}hrs ${sleepDuration[1]}mins")
        statsText.add("*Had an average heart rate of ${getData[3]}bpm")
        statsText.add("*Drank ${getData[4]} cups of water")
        //Get analysis of data
        val analysis = analysis(getData)
        //Merge the data and analysis strings together
        val fullText = combine(statsText, analysis)

        //Adds analysis to view
        for (x in 0 until fullText.size) {
            var textViewNew = TextView(this)
            textViewNew.text = fullText[x]
            textViewNew.textSize = 15F
            if (x%2 == 0) {
                textViewNew.setTypeface(null, Typeface.BOLD)
            }
            layout.addView(textViewNew)
        }

        //Return home if clicked
        btnHome.setOnClickListener {
            finish()
            val intent = Intent(this, MainActivity :: class.java)
            intent.putExtra("notification", "no")
            startActivity(intent)
        }

    }

    //Get data from previous day
    private fun readFile (dateSelected: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_DAY = 0
        val DATA_MONTH = 1
        val DATA_YEAR = 2
        val DATA_STEP_COUNT = 3
        val DATA_COUNT_SO_FAR = 4
        val DATA_SLEEP_TIME = 5
        val DATA_WAKE_TIME = 6
        val DATA_SLEEP_DURATION = 7
        val DATA_HEART_RATE = 8
        val DATA_WATER = 9

        try {
            val healthData = ArrayList<HealthData>()
            var dayLine: String?
            var dateDay:String = ""
            fileReader = BufferedReader(InputStreamReader(this.assets.open("health_data.csv")))
            fileReader.readLine() //Get rid of header
            dayLine = fileReader.readLine()
            while (dayLine != null) {
                val splitData = dayLine.split(",")
                if (splitData.isNotEmpty()) {
                    val dayData = HealthData(
                        splitData[DATA_DAY],
                        splitData[DATA_MONTH],
                        splitData[DATA_YEAR],
                        Integer.parseInt(splitData[DATA_STEP_COUNT]),
                        Integer.parseInt(splitData[DATA_COUNT_SO_FAR]),
                        splitData[DATA_SLEEP_TIME],
                        splitData[DATA_WAKE_TIME],
                        splitData[DATA_SLEEP_DURATION],
                        splitData[DATA_HEART_RATE],
                        splitData[DATA_WATER])
                    healthData.add(dayData)
                    dateDay = if (dateSelected[0] == '0') {
                        dateSelected[1].toString()
                    } else {
                        dateSelected[0].toString() + dateSelected[1].toString()
                    }
                    if (dayData.day == dateDay) {
                        arrayReturn.add(splitData[DATA_STEP_COUNT])
                        arrayReturn.add(splitData[DATA_SLEEP_TIME])
                        arrayReturn.add(splitData[DATA_SLEEP_DURATION])
                        arrayReturn.add(splitData[DATA_HEART_RATE])
                        arrayReturn.add(splitData[DATA_WATER])
                        return arrayReturn
                    }
                }
                dayLine = fileReader.readLine()
            }
        } catch (e: Exception) {
            println(e.printStackTrace())
        }
        return arrayReturn
    }

    //Split duration into hours and minutes
    private fun processTime(time: String) : MutableList<String> {
        var hours: String
        var mins: String
        var arrayReturn = mutableListOf<String>()

        var splitString: List<String> = time.split(":")
        if (splitString[0][0] == '0') {
            hours = if (splitString[0][1] == '0') {
                '0'.toString()
            } else {
                splitString[0][1].toString()

            }
        }
        else {
            hours = splitString[0]
        }
        if (splitString[1][0] == '0') {
            mins = if (splitString[1][1] == '0') {
                '0'.toString()
            } else {
                splitString[1][1].toString()
            }
        }
        else {
            mins = splitString[1]
        }
        arrayReturn.add(hours)
        arrayReturn.add(mins)
        return arrayReturn
    }

    //Analyse the data and add relevant comments to display to user
    private fun analysis(data: MutableList<String>): MutableList<String> {
        var arrayReturn = mutableListOf<String>()

        //steps
        if (data[0].toInt() < STEPS_BAD) {
            arrayReturn.add(getString(R.string.steps_bad))
        }
        else if (data[0].toInt() in (STEPS_BAD) until STEPS_GOOD) {
            arrayReturn.add(getString(R.string.steps_average))
        }
        else if (data[0].toInt() in (STEPS_GOOD) until STEPS_EXCELLENT) {
            arrayReturn.add(getString(R.string.steps_good))
        }
        else {
            arrayReturn.add(getString(R.string.steps_excellent))
        }

        //sleep time
        val splitTime: List<String> = data[1].split(":")
        val time = splitTime[0].toInt()
        if (time in 0 until 12) {
            arrayReturn.add(getString(R.string.sleep_time_bad))
        }
        else {
            arrayReturn.add(getString(R.string.sleep_time_good))
        }

        //sleep duration
        val splitDuration: List<String> = data[2].split(":")
        val duration = splitDuration[0].toInt()
        if (duration < SLEEP_TIME_BAD_UNDER) {
            arrayReturn.add(getString(R.string.sleep_bad_under))
        }
        else if (duration > SLEEP_TIME_BAD_OVER) {
            arrayReturn.add(getString(R.string.sleep_bad_over))
        }
        else {
            arrayReturn.add(getString(R.string.sleep_good))
        }

        //heart rate
        if (data[3].toInt() > HEART_RATE_BAD) {
            arrayReturn.add(getString(R.string.heart_bad))
        }
        else if (data[3].toInt() in HEART_AVG_BAD+1 until HEART_RATE_BAD+1) {
            arrayReturn.add(getString(R.string.heart_avg_bad))
        }
        else if (data[3].toInt() in HEART_AVG_GOOD until HEART_AVG_BAD+1) {
            arrayReturn.add(getString(R.string.heart_avg_good))
        }
        else {
            arrayReturn.add(getString(R.string.heart_very_low))
        }

        //water
        if (data[4].toInt() < WATER) {
            arrayReturn.add(getString(R.string.water_bad))
        }
        else {
            arrayReturn.add(getString(R.string.water_good))
        }

        return arrayReturn
    }

    //combining the stats and analysis together
    private fun combine(stats: MutableList<String>, analysis: MutableList<String>): MutableList<String> {
        var returnArray = mutableListOf<String>()

        for (i in 0 until 5) {
            returnArray.add(stats[i])
            returnArray.add(analysis[i])
        }
        return returnArray
    }
}
