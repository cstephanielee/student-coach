package com.example.studentcoach

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import io.realm.Realm
import io.realm.kotlin.where
import java.io.BufferedReader
import java.io.InputStreamReader

class SocialFragment : Fragment() {

    private var dateSelected:String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_social, container, false)

        //Get instance of database to use
        val realm = Realm.getInstance(RealmUtility.getDefaultConfig())
        for (date in realm.where<DatePicked>().findAll()) {
            dateSelected = date?.dateChosen.toString()
        }

        //Initialise widgets
        val dateText = view.findViewById<TextView>(R.id.date_text)
        val waterImage = view.findViewById<ImageView>(R.id.water_image)
        val forestInfoImage = view.findViewById<Button>(R.id.forest_info_image)
        val forestInfoText = view.findViewById<TextView>(R.id.forest_info_text)
        val waterInfoImage = view.findViewById<Button>(R.id.water_info_image)
        val waterInfoText = view.findViewById<TextView>(R.id.water_info_text)
        var linearLayout = view.findViewById<LinearLayout>(R.id.Forest_Layout)
        val treeCountText = view.findViewById<TextView>(R.id.tree_count_text)
        val btnBadges =view.findViewById<Button>(R.id.badge_button)
        val btnFriends = view.findViewById<Button>(R.id.friends_button)

        dateText.text = "Data from: $dateSelected"
        //Get data for current date
        val data = getData(dateSelected)

        //Making Forest
        val countSoFar = data[1]
        val amountOfTrees = countSoFar.toInt()/10000

        for (x in 1 until amountOfTrees+1) {
            val treeImage = ImageView(activity)
            treeImage.layoutParams = LinearLayout.LayoutParams(160,160)
            if (x%2 == 0) {
                treeImage.setImageResource(R.drawable.tree)
            }
            else {
                treeImage.setImageResource(R.drawable.tree2)
            }
            linearLayout.addView(treeImage)
        }
        treeCountText.text = "Total Trees: $amountOfTrees"

        //Filling up glass with water
        if (dateSelected != "") {
            val waterCount = data[0].toInt()
            when (waterCount) {
                0 -> waterImage.setImageResource(R.drawable.glass0)
                1 -> waterImage.setImageResource(R.drawable.glass1)
                2 -> waterImage.setImageResource(R.drawable.glass2)
                3 -> waterImage.setImageResource(R.drawable.glass3)
                4 -> waterImage.setImageResource(R.drawable.glass4)
                5 -> waterImage.setImageResource(R.drawable.glass5)
                6 -> waterImage.setImageResource(R.drawable.glass6)
                7 -> waterImage.setImageResource(R.drawable.glass7)
                else -> {
                    waterImage.setImageResource(R.drawable.glass8)
                }
            }
        }

        //View users badges activity if clicked
        btnBadges.setOnClickListener {
            activity?.finish()
            val intent = Intent(activity, BadgeActivity::class.java)
            intent.putExtra("name", "me")
            intent.putExtra("Return", "Social")
            startActivity(intent)
        }

        //Go to Leader Board page if clicked
        btnFriends.setOnClickListener {
            activity?.finish()
            startActivity(Intent(activity, FriendsActivity::class.java))
        }

        //Display and hide information
        forestInfoImage.setOnClickListener {
            forestInfoText.visibility =
                if (forestInfoText.visibility == View.VISIBLE) {
                    View.INVISIBLE
                } else {
                    View.VISIBLE
                }
        }
        //Display and hide information
        waterInfoImage.setOnClickListener {
            waterInfoText.visibility =
                if (waterInfoText.visibility == View.VISIBLE) {
                    View.INVISIBLE
                } else {
                    View.VISIBLE
                }
        }
        return view
    }

    //Get data for current day
    private fun getData (dateSelected: String) : MutableList<String> {
        var fileReader: BufferedReader?
        var arrayReturn = mutableListOf<String>()

        val DATA_DAY = 0
        val DATA_MONTH = 1
        val DATA_YEAR = 2
        val DATA_STEP_COUNT = 3
        val DATA_COUNT_SO_FAR = 4
        val DATA_SLEEP_TIME = 5
        val DATA_WAKE_TIME = 6
        val DATA_SLEEP_DURATION = 7
        val DATA_HEART_RATE = 8
        val DATA_WATER = 9

        try {
            val healthData = ArrayList<HealthData>()
            var dayLine: String?
            var dateDay:String = ""
            fileReader = BufferedReader(InputStreamReader(context!!.assets.open("health_data.csv")))
            fileReader.readLine() //Get rid of header
            dayLine = fileReader.readLine()
            while (dayLine != null) {
                val splitData = dayLine.split(",")
                if (splitData.isNotEmpty()) {
                    val dayData = HealthData(
                        splitData[DATA_DAY],
                        splitData[DATA_MONTH],
                        splitData[DATA_YEAR],
                        Integer.parseInt(splitData[DATA_STEP_COUNT]),
                        Integer.parseInt(splitData[DATA_COUNT_SO_FAR]),
                        splitData[DATA_SLEEP_TIME],
                        splitData[DATA_WAKE_TIME],
                        splitData[DATA_SLEEP_DURATION],
                        splitData[DATA_HEART_RATE],
                        splitData[DATA_WATER])
                    healthData.add(dayData)
                    if (dateSelected[0] == '0') {
                        dateDay = dateSelected[1].toString()
                    }
                    else {
                        dateDay = dateSelected[0].toString() + dateSelected[1].toString()
                    }
                    if (dayData.day == dateDay) {
                        arrayReturn.add(splitData[DATA_WATER])
                        arrayReturn.add(splitData[DATA_COUNT_SO_FAR])
                        return arrayReturn
                    }
                }
                dayLine = fileReader.readLine()
            }

            for (dayData in healthData) {
                println(dayData)
            }
        } catch (e: Exception) {
            println(e.printStackTrace())
        }
        return arrayReturn
    }

}

