package com.example.studentcoach

import io.realm.RealmConfiguration


object RealmUtility {
    private const val SCHEMA_V_PREV = 1 // previous schema version
    const val schemaVNow = 2 // change schema version if any changes happen in schema

    fun getDefaultConfig(): RealmConfiguration? {
        return RealmConfiguration.Builder()
            .schemaVersion(schemaVNow.toLong())
            .deleteRealmIfMigrationNeeded() // if migration needed then this will remove the existing database and create a new database
            .build()
    }
}